
var lol = document.getElementById('barChart').getContext('2d');
var myChart = new Chart(lol, {
  type: 'bar',
  data: {
    labels: ['JAN', 'FEB', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JUL'],
    datasets: [
    {
      label: 'Borrowed Books',
      data: [13, 9, 7, 11, 12, 10 , 9],
      backgroundColor: 'rgba(244, 148, 148, .9)',
    },
    {
      label: 'Returned Books',
      data: [7, 11, 5, 9, 8, 8, 5],
      backgroundColor: 'rgba(255, 191, 96, .9)',
    }
  ],
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});

var pie = document.getElementById('pieChart').getContext('2d');
var pieChart = new Chart(pie, {
  type: 'doughnut',
  data: {
    datasets: [{
        data: [10, 20, 30, 20 ,20],
        backgroundColor: [
          'rgba(148, 244, 198, .9)',
          'rgba(61, 173, 255, .9)',
          'rgba(249, 174, 106, .9)',
          'rgba(255, 31, 31, .9)',
          'rgba(209, 104, 245, .9)',
        ],
    }],

    labels: [
        'Java',
        'Phyton',
        'C++',
        'HTML',
        'CSS',
    ]
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});


