<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Http\Requests\BookRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class BookController extends Controller
{
    public function index()
    {
        return response()->json(Book::with(['category:id,category'])->get());
    }
    public function store(BookRequest $request)
    {
        return response()->json(Book::create($request->all()));
    }
    public function show($id)
    {
        try{
            $book = Book::with(['category:id,category'])->where('id', $id)->firstOrFail();
            return response()->json($book);
        }catch(ModelNotFoundException $exception){
            return response()->json(['message' => 'Book not found']);
        }
    }
    public function update(BookRequest $request, $id)
    {
        $book = Book::with(['category:id,category'])->where('id', $id)->firstOrFail();
        $book->update($request->all());
        return response()->json(['message' => 'Book updated successfully!', 'book' => $book]);
    }
    public function destroy($id)
    {
        try{
            $book = Book::where('id', $id)->firstOrFail();
            $book->delete();
            return response()->json(['message' => 'Book deleted successfully!']);
        }catch(ModelNotFound $exception){
            return response()->json(['message' => 'Book not found']);
        }
    }
}
