<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReturnedBookRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules(Request $request)
    {
        $borrowed = BorrowedBook::where('book_id', $request->book_id)->where('patron_id', $request->patron_id)->get();
        if(empty($borrowed))
        {
            $copies = $request->copies;
        }
        else
        {
           $copies =  $borrowed->copies;
        }
        
        return [
            'book_id' => ['bail', 'required', 'exists:borrowed_books,book_id'],
            'copies' => ['integer', 'gt:0', "lte:{$copies}", 'required'],
            'patron_id' => ['exists:borrowed_books,patron_id']
        ];
    }

    public function messages(){
        return [
            'book_id.required' => 'Book id is required.',
            'book_id.exists' => 'Book id must exist in the borrowed books.',
            'copies.integer' => 'Copies must be integer',
            'patron_id.exists' => 'Patron id must exist on the patron table'
        ];
    }
}
