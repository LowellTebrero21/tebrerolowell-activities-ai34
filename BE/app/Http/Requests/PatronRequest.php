<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatronRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'last_name' => ['required', 'bail', 'max:255'],
            'first_name' => ['required', 'bail', 'max:255'],
            'middle_name' => ['required', 'bail', 'max:255'],
            'email' => ['email', 'required']
        ];
    }

    public function messages(){
        return [
            'last_name.required' => 'Last name is required',
            'middle_name.required' => 'Middle name is required',
            'first_name.required' => 'First name is required',
            'email.unique' => 'Email address of the patron must be unique'
        ];
    }
}
