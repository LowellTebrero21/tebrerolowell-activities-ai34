<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'bail', 'max:255'],
            'author' => ['required', 'bail', 'max:255'],
            'copies' => ['required', 'integer'],
            'category_id' => ['exists:categories,id', 'required', 'integer']
        ];
    }
}
