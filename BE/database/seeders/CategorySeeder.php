<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = [
            ['category' => 'Java'],
            ['category' => 'HTML'],
            ['category' => 'C++'],
            ['category' => 'C#'],
        ];

        foreach ($category as $categ) {
            Category::create($categ);
        }
    }
}
