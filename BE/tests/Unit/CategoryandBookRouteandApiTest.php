<?php

namespace Tests\Unit;

use App\Models\Book;
use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CategoryandBookRouteandApiTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_could_get_the_categories()
    {
        $this->call('GET', 'api/categories')->assertStatus(200);
    }

    public function test_could_access_and_get_all_the_book(){
        $this->call('GET', 'api/books')->assertStatus(200);
    }

    public function test_could_create_books()
    {
        $category = Category::create(['category' => 'Programming']);
        $book = Book::factory()->make(['category_id' => $category->id]);

        $this->call('POST', '/api/books', $book->toArray());
        $this->assertDatabaseHas('books', $book->toArray());
    }

    public function test_could_update_the_books()
    {
        $category = Category::create(['category' => 'C++ - 2']);
        $book = Book::factory()->create(['category_id' => $category->id]);
        $data = ['name' => 'CSS Basics - Updated', 'copies' => 15, 'category_id' => $category->id, 'author' => 'Tebrero'];
        
        $expected_result = ['message' => 'Book updated successfully!'];
        $this->call('PUT', '/api/books/'.$book->id, $data)->assertSee($expected_result);
    }

    public function test_could_delete_the_book()
    {
        $category = Category::create(['category' => 'C++ - 2']);
        $book = Book::factory()->create(['category_id' => $category->id]);

        $this->call('DELETE', '/api/books/'.$book->id);
        $this->assertDatabaseMissing('books', $book->toArray());
    }

    public function test_could_show_specified_book()
    {
        $category = Category::create(['category' => 'C++ - 2']);
        $book = Book::factory()->create(['category_id' => $category->id]);

        $this->call('GET', '/api/books/'.$book->id)->assertJson($book->toArray());
    }
}
