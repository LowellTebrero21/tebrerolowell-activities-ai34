<?php

use App\Models\Patron;
use Tests\TestCase;



class PatronRouteandApiTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_could_access_get_all_the_patron()
    {
        $this->call('GET', 'api/patrons')->assertStatus(200);
    }

    public function test_could_create_the_patron()
    {
        $patron = Patron::factory()->make();
        
        $this->call('POST', '/api/patrons', $patron->toArray())->assertSuccessful();
        $this->assertDatabaseHas('patrons', $patron->toArray());
    }

    public function test_could_update_the_patron()
    {
        $patron = Patron::factory()->create();
        $data = [
            'last_name' => 'Tebrero',
            'first_name' => 'Kuzma',
            'middle_name' => 'Simba',
            'email' => 'lowelltebrero@gmail.com'
        ];

        $this->call('PUT', '/api/patrons/'.$patron->id, $data);
        $this->assertDatabaseHas('patrons', $data);
    }

    public function test_could_delete_the_patron()
    {
        $patron = Patron::factory()->make();
        
        $this->call('DELETE', 'api/patrons/'.$patron->id);
        $this->assertDatabaseMissing('patrons', $patron->toArray());
    }

    public function test_could_show_specified_patron()
    {
        $patron = Patron::factory()->create();
        
        $this->call('GET', '/api/patrons/'.$patron->id)->assertJson($patron->toArray());
    }
}
