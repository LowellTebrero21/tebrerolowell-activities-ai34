<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;

class JWTAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        try {

            $user = JWTAuth::parseToken()->authenticate();

        }catch(Exception $e) {
            if($e instanceof TokenEx) {
                $newToken = JWTAuth::refresh(JWTAuth::getToken());     
                return response()->json([ 'isExpired' => true,'newToken' => $newToken], 200);
            }else if($e instanceof TokenInvalidException) {
                return response()->json(['message' => 'Token Invalid'], 401);
            }else {
                return response()->json(['message' => 'Token Not Found'], 401);
            }
        }

        return $next($request);
    }
}
