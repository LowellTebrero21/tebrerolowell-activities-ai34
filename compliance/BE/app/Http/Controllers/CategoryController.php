<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;
use Laravel\Ui\Presets\React;

class CategoryController extends Controller
{
    //
    /**
     * Create a new CategoryController instance.
     *
     * @return void
     */
     public function __construct()
     {
         $this->middleware('auth:api');
     }
 
     public function get(Request $request) {
         return Category::where('user_id', auth('api')->user()->id)->paginate(5);
     }
 
     public function total(Request $request) {
         return response()->json(['total_categories' => Category::all()->where('user_id', auth('api')->user()->id)->count()]);
     }
 
     public function store(Request $request) {
         
         $validate = Validator::make($request->all(), [
             'category.name'  => 'required',
             
         ]);
         if ($validate->fails())
         {
             $message = $validate->errors()->getMessages();
 
             return response()->json([
                 'success' => false,
                 'messages' => $message 
             ], 200);
         }
         $category = Category::create([
             'user_id' => auth('api')->user()->id,
             'name' => $request->category['name'],
         ]);
 
         return response()->json(['success' => true, 'message' => 'Category Added!', 'category' => $category], 200);
     }
 
     public function delete(Request $request) {
         Category::where('id', $request->id)->delete();
         return response()->json([ 'message' => 'Category Deleted!'], 200);
     }
 
     public function update(Request $request)
     {
         $validate = Validator::make($request->all(), [
             'category.name'  => 'required',
             
         ]);
         if ($validate->fails())
         {
             $message = $validate->errors()->getMessages();
 
             return response()->json([
                 'success' => false,
                 'messages' => $message 
             ], 200);
         }
         $category = [
             'name' => $request->category['name'],
         ];
         Category::where('id', $request->category['id'])->update($category);
 
         return response()->json(['success' => true, 'message' => 'Category Updated!', 'category' => $category], 200);
     }
}
