<?php

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    //

    /** 
     * Create a new AuthController instance.
     *
     * @return void
     */
     public function __construct()
     {
         $this->middleware('auth:api', ['except' => ['login', 'register']]);
     }
 
 
     public function register(Request $request)
     {
         $validate = Validator::make($request->all(), [
             'name'  => 'required|min:6',
             'username' => 'required|min:6|unique:users',
             'password'  => 'required|min:6|confirmed',
             
         ]);
         if ($validate->fails())
         {
             return response()->json([
                 'success' => false,
                 'status' => 'error',
                 'errors' => $validate->errors()
             ], 200);
         }
         
         User::create([
             'name' => $request->name,
             'username' => $request->username,
             'password' => bcrypt($request->password),
         ]);
 
         return response()->json(['success' => true, 'message' => 'Account Registered'], 200);
     }
 
     /**
      * Get a JWT token via given credentials.
      *
      * @param  \Illuminate\Http\Request  $request
      *
      * @return \Illuminate\Http\JsonResponse
      */
     public function login(Request $request)
     {
         $credentials = $request->only('username', 'password');
 
         if (! $token = auth()->attempt($credentials)) {
             return response()->json(['success' => false, 'message' => 'Invalid Cridentials'], 200);
         }
 
         return $this->respondWithToken($token);
     }
 
     /**
      * Refresh a token.
      *
      * @return \Illuminate\Http\JsonResponse
      */
     public function refresh()
     {
         return $this->respondWithToken(auth('api')->refresh());
     }
 
     public function user()
     {
         return response()->json(auth('api')->user(), 200);
     }
 
     /**
      * Log the user out (Invalidate the token)
      *
      * @return \Illuminate\Http\JsonResponse
      */
     public function logout()
     {
         auth()->logout();
        return response()->json(['message' => 'Your account sucessfully logged out!'], 200); 
     }
 
     public function verifyToken() {
         return response()->json(['hasToken'=> true], 200);
     }
 
     protected function respondWithToken($token)
     {
         return response()->json([
                 'message' => 'You successfully logged in!',
                 'success' => true,
                 'token' => $token,
                 'token_type' => 'bearer',
                 'expires_in' => auth()->factory()->getTTL() * 60
         ]);
     }
}
