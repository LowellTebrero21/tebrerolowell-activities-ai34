import Vue from 'vue'
import App from './App.vue'
import router from "./router";
import store from "./store";
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-icons/font/bootstrap-icons.css'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
const options = {
  transition: "Vue-Toastification__fade",
  maxToasts: 5,
  newestOnTop: true
};

Vue.config.productionTip = false
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.use(Toast, options);
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
new Vue({
  router, store,
  render: h => h(App),
}).$mount('#app')
