import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import Login from '../components/views/Login'
import Register from '../components/views/Register'
import Dashboard from '../components/views/Dashboard'
import Category from '../components/views/Category'
import Inventory from '../components/views/Inventory'
import NotFound from '../components/views/NotFound'

const routes = [
    {
        path: '*',
        name: 'NotFound',
        component: NotFound
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/register',
        name: 'Register',
        component: Register
    },
    {
        path: '/',
        name: 'Dashboard',
        component: Dashboard
    },
    {
        path: '/category',
        name: 'Category',
        component: Category
    },
    {
        path: '/inventory',
        name: 'Inventory',
        component: Inventory
    }
]

const router = new VueRouter({
 history: true,
 mode: "history",
 routes
});


export default router