import Vuex from "vuex";
import Vue from "vue";
import products from "./modules/products";
import categories from "./modules/category";
import user from "./modules/user";
import token from "./modules/token";
import utils from "./modules/utils";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
      products,
      categories,
      user,
      token,
      utils
    }
});