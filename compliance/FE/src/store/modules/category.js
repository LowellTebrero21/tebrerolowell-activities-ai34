import API from '../base'

const state = {
 categories: {},
 totalCategories: 0,
 message: "",
 isSuccess: false
};

const actions = {
 async fetchTotalCategories(state) {
     const response = await API.get(`category/total?token=${localStorage.getItem('access_token')}`);
     state.commit("setTotalCategories", response.data);
 },
 async fetchCategories(state, page=1) {
     const response = await API.get(
         `category/get?page=${page}&token=${localStorage.getItem('access_token')}`
     );
     state.commit("setCategories", response.data);
 },
 async addCategory(state, category) {
     await API
         .post(`category/store?token=${localStorage.getItem('access_token')}`, {
             category: category
         })
         .then(res => {
             if (res.data.success)
                 return state.commit("setResponse", res.data);
             state.commit("setResponse", res.data);
         });
 },
 async updateCategory(state, category) {
     await API
         .put(`category/update?token=${localStorage.getItem('access_token')}`, {
             category: category
         })
         .then(res => {
             if (res.data.success)
                 return state.commit("setResponse", res.data);
             state.commit("setResponse", res.data);
         });
 },
 async deleteCategory(state, id) {
     const response = await API.delete(
         `category/delete/${id}?token=${localStorage.getItem('access_token')}`
     );
     state.commit("setMessage", response.data.message);
 }
};

const getters = {
 //Category
 getCategories: state => state.categories,
 getTotalCategories: state => state.totalCategories,
 getMessageCategory: state => state.message,
 getSuccessCategory: state => state.isSuccess
};

const mutations = {
 //Category
 setCategories: (state, categories) => (state.categories = categories),
 setTotalCategories: (state, category) =>
     (state.totalCategories = category.total_categories),
 //Message
 setResponse: (state, response) => {
     state.isSuccess = response.success;
     state.message = state.isSuccess ? response.message : response.messages;
 },
 setMessage: (state, message) => (state.message = message),
 clearMessage: state => (state.message = "")
};

export default {
 namespaced: true,
 state,
 getters,
 actions,
 mutations
};
