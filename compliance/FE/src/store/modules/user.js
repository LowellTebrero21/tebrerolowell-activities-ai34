import API from '../base'
const state = {
 user: [],
 token: localStorage.getItem('access_token'),
};

const getters = {
 getUser: state => state.user
};

const actions = {
 async fetchUser(state) {
     const response = await API.get(`auth/user?token=${localStorage.getItem('access_token')}`, {
     });
     state.commit("setUser", response.data);
 }
};

const mutations = {
 setUser: (state, user) => (state.user = user)
};

export default {
 namespaced: true,
 state,
 getters,
 actions,
 mutations
};
