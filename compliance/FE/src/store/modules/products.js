import API from '../base'

const state = {
 products: {},
 totalProduct: 0,
 outOfStock: 0,
 message: "",
 isSuccess: false,
 isLoading: true,
 isDisabled: false
};

const getters = {
 getProducts: state => state.products,
 getTotalProduct: state => state.totalProduct,
 getOutOfStock: state => state.outOfStock,
 getMessageProduct: state => state.message,
 getSuccessProduct: state => state.isSuccess,
};

const actions = {
 //Product
 async fetchTotalProduct(state) {
     const response = await API.get(`product/total?token=${localStorage.getItem('access_token')}`);
     state.commit("setTotalProduct", response.data);
 },
 async fetchOutOfStock(state) {
     const response = await API.get(`/product/out_of_stock?token=${localStorage.getItem('access_token')}`);
     state.commit("setOutOfStock", response.data);
 },
 async fetchProducts(state, page = 1) {
     const response = await API.get( `product/get?&page=${page}&token=${localStorage.getItem('access_token')}`);
     state.commit("setProducts", response.data);
 },
 async addProduct(state, product) {
     await API
         .post(`/product/store?token=${localStorage.getItem('access_token')}`, {
             product: product
         })
         .then(res => {
             if (res.data.success)
                 return state.commit("setResponse", res.data);
             state.commit("setResponse", res.data);
         });
 },
 async updateProduct(state, product) {
     await API
         .put(`/product/update?token=${localStorage.getItem('access_token')}`, {
             product: product
         })
         .then(res => {
             if (res.data.success)
                 return state.commit("setResponse", res.data);
             state.commit("setResponse", res.data);
         });
 },
 async deleteProduct(state, id) {
     console.log(id)
     const response = await API.delete(
         `/product/delete/${id}?token=${localStorage.getItem('access_token')}`
     );
     state.commit("setMessage", response.data.message);
     
 }
};
const mutations = {
 setProducts: (state, products) => (state.products = products),
 setTotalProduct: (state, product) =>
     (state.totalProduct = product.total_products),
 setOutOfStock: (state, product) =>
     (state.outOfStock = product.out_of_stock),
 setResponse: (state, response) => {
     state.isSuccess = response.success;
     state.message = state.isSuccess ? response.message : response.messages;
 },
 setMessage: (state, message) => (state.message = message),
 clearMessage: state => (state.message = ""),
};

export default {
 namespaced: true,
 state,
 getters,
 mutations,
 actions
};
