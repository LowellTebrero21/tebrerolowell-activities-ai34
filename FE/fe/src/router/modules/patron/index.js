import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '../components/pages/dashboard/dashboard'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Dashboard',
        component: Dashboard
    },
    {
        path: '/settings',
        name: 'Settings',
        component: () => import(/* webpackChunkName: "settings" */ '@/components/pages/setting/settings')
    },
    {
        path: '/books',
        name: 'Books Management',
        component: () => import(/* webpackChunkName: "books" */ '@/components/pages/books/books')
    },
    {
        path: '/patron',
        name: 'Patron Management',
        component: () => import(/* webpackChunkName: "patron" */ '@/components/pages/patron/patron')
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router