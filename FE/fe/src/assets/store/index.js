import Vue from 'vue'
import Vuex from 'vuex'
import book from './modules/book'
import borrowed from './modules/borrowed'
import patron from './modules/patron'
import returned from './modules/returned'



Vue.use(Vuex)

export default new Vuex.Store({ modules: { book, borrowed, patron, returned } })